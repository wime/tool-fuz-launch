from dataclasses import dataclass
import json
import os
import subprocess
from typing import Callable

from .fuzzel import run_fuzzel


def set_homedir(cmd: str) -> str:
    """Change '~' to home-dir path in executable string."""
    if cmd.startswith("~"):
        cmd = cmd.replace("~", os.environ["HOME"])
    return cmd


def cmd_to_fun(cmd: str) -> Callable:
    """Get executable function from shell-command."""
    def fun() -> None:
        subprocess.run(set_homedir(cmd), shell=True)
    return fun


@dataclass
class ListEntry:
    icon: str
    text: str
    cmd: Callable
    confirmation: bool = False

    def __repr__(self) -> str:
        return f"{self.icon:^3} {self.text}"
    

@dataclass
class MenuList:
    entries: list[ListEntry]
    # passw: bool = False
    prompt: str = "> "
    chosen_entry: str | None = None

    def __add__(self, other) -> "MenuList":
        return MenuList(self.entries + other.entries)

    def __repr__(self) -> str:
        return "\n".join([str(entry) for entry in self.entries])

    def get_list_entries(self):
        return [str(entry) for entry in self.entries]
    
    def get_entry(self, text) -> ListEntry | None:
        entries_by_text = {entry.text: entry for entry in self.entries}
        return entries_by_text.get(text)
    
    def sort_by_file(self, file: str):
        file = file.replace("~", os.environ["HOME"]) 
        if not os.path.exists(file):
            return

        try:
            with open(file, "r") as f:
                history = json.load(f)
        except Exception:
            return
        sorted_history = [k for k, v in sorted(history.items(), key=lambda item: item[1], reverse=True)]
        sorted_entries = []
        for item in sorted_history:
            for entry in self.entries:
                if item == entry.text:
                    sorted_entries.append(entry)
                    continue
        for entry in self.entries:
            if entry not in sorted_entries:
                sorted_entries.append(entry)
        self.entries = sorted_entries

    def run(self) -> None:
        result = run_fuzzel(
            entries=self.get_list_entries(),
            # passw=self.passw,
            prompt=self.prompt

        ).lstrip(" ")
        result = " ".join(result.split(" ")[1:]).lstrip(" ")
        chosen_entry = self.get_entry(result)
        if not chosen_entry:
            return
        self.chosen_entry = chosen_entry.text
        if chosen_entry.confirmation:
            confirmation_menu = get_confirmation_menu(entry=chosen_entry)
            confirmation_menu.run()
            return
        chosen_entry.cmd()

    def store(self, file: str) -> None:
        file = file.replace("~", os.environ["HOME"]) 
        ranking = {entry.text: 0 for entry in self.entries}
        path = "/".join(file.split("/")[:-1])
        if not os.path.exists(path):
            os.makedirs(path)
        if os.path.exists(file):
            try:
                with open(file, "r") as f:
                    history = json.load(f)
            except Exception:
                pass
            else:
                history = {k: v for k,v in history.items() if k in ranking.keys()}
                ranking.update(history)
        if self.chosen_entry:
            ranking[self.chosen_entry] += 1

        with open(file, "w") as f:
            json.dump(ranking, f, indent=4)


def get_confirmation_menu(entry: ListEntry, cancel_icon: str = "") -> MenuList:
    def exec_cancelled():
        return None
    return MenuList(
        entries=[
            ListEntry(icon=entry.icon, text=entry.text, cmd=entry.cmd),
            ListEntry(icon=cancel_icon, text="Cancel", cmd=exec_cancelled)
        ]
    )

def entry_from_json(json: dict) -> ListEntry:
    return ListEntry(
        text=json["text"],
        icon=json["icon"],
        cmd=cmd_to_fun(json["exec"]),
        confirmation=json.get("confirmation", False)
    )

def menu_from_json(json: dict) -> MenuList:
    entries = [entry_from_json(json=entry) for entry in json]
    return MenuList(entries=entries)
