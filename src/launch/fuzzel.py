import subprocess


def run_fuzzel(
    entries: list[str],
    # passw: bool = False,
    prompt: str = "> "
) -> str:
    """Run fuzzel."""
    lines_input = f" -l {len(entries)}" if len(entries) < 10 else ""
    # passw_input = f" --password" if passw else ""
    entry_lines = "\n".join(entries)
    # fuzzel_run = f"fuzzel -d{lines_input}{passw_input} -p '{prompt}'"
    fuzzel_run = f"fuzzel -d{lines_input} -p '{prompt}'"
    result = subprocess.run(
        f"echo -e '{entry_lines}' | {fuzzel_run}",
        shell=True,
        capture_output=True,
    )
    return result.stdout.decode("utf-8").rstrip("\n")
