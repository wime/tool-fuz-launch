from dataclasses import dataclass
import os

from .launch_classes import ListEntry, MenuList, cmd_to_fun


@dataclass
class App:
    name: str | None = None
    command: str | None = None
    icon: str | None = None
    noshow: bool = False

    def text_icon(self, config: dict):
        application = None
        apps_by_names = {x["name"].lower(): x for x in config["apps"]}
        application = apps_by_names.get(self.icon)
        if not application:
            apps_by_long_names = {x.get("long_name", ""): x for x in config["apps"]}
            application = apps_by_long_names.get(self.icon)
        if not application:
            apps_by_long_names = {x.get("long_name2", ""): x for x in config["apps"]}
            application = apps_by_long_names.get(self.icon)
        if not application:
            return config["icons"]["default"]
        icon_category = application["icon"]
        if not application:
            return config["icons"]["default"]
        return config["icons"].get(icon_category, config["icons"]["default"])

        # if not self.icon:
        #     return icon_config["Categories"]["default"]
        # category_name = icon_config["Apps"].get(self.icon, "default")
        # return icon_config["Categories"].get(category_name, icon_config["Categories"]["default"])


    def to_list_entry(self, config: dict) -> ListEntry | None:
        if self.icon == "foot":
            print(self)
        if not self.noshow:
            return ListEntry(
                icon=self.text_icon(config),
                text=self.name,
                cmd=cmd_to_fun(self.command)
            )


def line_is_section_name(line: str) -> bool:
    return line.startswith("[") and line.endswith("]")


def parse_file(file: list[str]) -> dict:
    section = False
    elements = {}
    file = [line for line in file if line.lstrip(" ")[0] not in ["#", "\n"]]

    for line in file:
        line = line.rstrip("\n").lstrip(" ").rstrip(" ")
        if line[0] == "#" or line == "":
            continue
        if section and "=" in line:
            splitted_line = line.split("=")
            key = splitted_line[0]
            if key not in elements.keys():
                elements[key] = "=".join(splitted_line[1:]).removesuffix("\n")
        elif section:
            break
        if "[Desktop Entry]" in line:
            section = True
    return elements


def get_desktop_file(path: str) -> dict:
    with open(path, "r") as f:
        lines = f.readlines()
    return parse_file(lines)


def get_app_info(config: dict, app: App) -> App:
    app.name = config.get("Name", app.name)
    app.command = config.get("Exec", app.command)
    if app.command:
        app.command = app.command.split("%u")[0].split("%U")[0]
    app.icon = config.get("Icon", app.icon)
    if "NoDisplay" in config.keys():
        app.noshow = "true" in config["NoDisplay"]
    elif "NotShowIn" in config.keys():
        app.noshow = "GNOME" in config["NotShowIn"]
    elif "OnlyShowIn" in config.keys():
        app.noshow = "GNOME" not in config["OnlyShowIn"]
    return app



def loop_though_flatpaks() -> list[App]:
    flatpak_dir = "/var/lib/flatpak/exports/share/applications"
    apps = []
    for app_file in os.listdir(flatpak_dir):
        desktop_file = f"{flatpak_dir}/{app_file}"
        if not os.path.isfile(desktop_file):
            continue
        file = get_desktop_file(desktop_file)
        app = get_app_info(file, App())
        if not app.noshow:
            apps.append(app)
    return apps


def loop_though_rpms():
    rpm_dir = "/usr/share/applications"
    user_dir = os.environ["HOME"] + "/.local/share/applications"
    apps = []
    for d_file in os.listdir(rpm_dir):
        if d_file.split(".")[-1] != "desktop":
            continue
        desktop_file = f"{rpm_dir}/{d_file}"
        user_desktop_file = f"{user_dir}/{d_file}"
        if not os.path.isfile(desktop_file):
            continue
        file = get_desktop_file(desktop_file)
        app = get_app_info(file, App())
        if os.path.isfile(user_desktop_file):
            file = get_desktop_file(user_desktop_file)
            app = get_app_info(file, app)
        if not app.noshow:
            apps.append(app)
    return apps


def get_apps(config: dict):
    rpms = loop_though_rpms()
    flatpaks = loop_though_flatpaks()
    # config = get_config()
    return MenuList(
        entries=[app.to_list_entry(config) for app in rpms+flatpaks]
    )

