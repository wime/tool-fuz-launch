import argparse
import os
import tomllib
import json

from .launch_classes import menu_from_json
from .apps import get_apps


def get_args():
    parser = argparse.ArgumentParser(
        prog='launch',
        description="Launcher using fuzzel.",
    )
    parser.add_argument("-c", "--config", default="~/.config/launch/launch.toml", required=False)
    return parser.parse_args()


def read_config(file: str) -> dict | None:
    file = file.replace("~", os.environ["HOME"])
    try:
        with open(file, "rb") as f:
            data = tomllib.load(f)
    except FileNotFoundError:
        print(f"`{file}` does not exist!")
    else:
        return data

def get_config():
    path = f"{os.environ['HOME']}/.config/icons/apps.json"
    with open(path) as json_data:
        conf = json.load(json_data)
    return conf

def main():
    state_file = "~/.local/state/launch/count.json"
    args = get_args()
    config = read_config(file=args.config)
    if not config:
        return
    menu = menu_from_json(json=config["entries"])
    if config.get("apps", {}).get("show", False):
        apps = get_apps(get_config())
        menu += apps
        
    menu.prompt = config.get("main", {}).get("prompt", menu.prompt)
    menu.sort_by_file(state_file)
    menu.run()
    menu.store(state_file)


if __name__ == "__main__":
    main()
    
