# Description
This is a tool for launching custom entries with [fuzzel](https://codeberg.org/dnkl/fuzzel).

The tool was made for making my own life easier. Fuzzel is a great launcher, and this tool makes it simpler to write a config-file as input to Fuzzel.

# Getting Started
## Installation
Install with:
```sh
pip install fuz-launch
```
Or from within a Python virtual environment:
```sh
pip install --user fuz-launch
```
## Usage
Run the launcher with:
```sh
launch
```
or
```sh
python -m launch
```

# Configuration
The tool needs a configuration file located at `~/.config/launch/config.toml`.
The config file has three sections: main, apps and entries.

## main
The main section contains the following options:
* `prompt` {`str`} -- The chosen prompt.
    - Default: `"> "`

## apps
The apps section contains the following options:
* `show` {`bool`} -- Should apps be shown in the launcher. If true, apps are read from the `.desktop` files.
    - Default: `true`

## entries
Entries is a list of entries to be shown in the launcher. Each entry has the following options:
* `text` {`str`} -- Text to be shown in the entry.
    - Required: `yes`
* `icon` {`str`} -- The (text-based) icon to be shown in front of the text.
    - Default: `""`
* `exec` {`str`} -- The shell command to execute if chosen.
    - Required: `yes`
* `confirmation` {`bool`} -- If a confirmation menu should be shown after choosing the entry.
    - Default: `false`

## Example
```toml
[main]
prompt = "> "

[apps]
show = true

[[entries]]
[[entries]]
icon = ""
text = "Poweroff"
exec = "systemctl poweroff"
confirmation = true
[[entries]]
text = "Reboot"
icon = ""
exec = "systemctl reboot"
confirmation = true
```

# License
The tool is published under the MIT license.

